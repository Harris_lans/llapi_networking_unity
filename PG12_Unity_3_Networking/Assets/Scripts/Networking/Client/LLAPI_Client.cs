﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace LLAPI
{
	public class LLAPI_Client : MonoBehaviour 
	{

		#region Globals
			[SerializeField]
			private string _ServerAddress = "127.0.0.1";

			[SerializeField]
			private int _ServerPort = 27000;

			[SerializeField]
			private int _BufferSize = 1024;

			[Space, Header("User Details")]
			public String Username = "Harish";

			[SerializeField]
			private SO_NetMessageContainer _NetMessageContainer;

			[Range(0, 1)]
			public int TeamNumber = 0;

			private byte _ThreadPoolSize = 4;
			public int SocketID {get; private set;}
			public int ServerConnectionID {get; private set;}
			public byte ReliableChannel {get; private set;}
			public byte UnreliableChannel {get; private set;}
			public Dictionary<int, NetUser> NetUsers = new Dictionary<int, NetUser>();

		#endregion

		#region Life Cycle
		
			void Start()
			{
				ConnectToServer();
				StartCoroutine(Recevier());
			}

			void OnDisable()
			{
				DisconnectFromServer();
			}

			private IEnumerator Recevier()
			{
				int recieveSocketID, recieveConnectionID, recieveChannelID, recieveDataSize;
				byte error;
				byte[] recieveBuffer = new byte[_BufferSize];

				while(true)
				{
					NetworkEventType NEType = NetworkTransport.Receive
					(
						out recieveSocketID, 
						out recieveConnectionID, 
						out recieveChannelID, 
						recieveBuffer, 
						_BufferSize, 
						out recieveDataSize, 
						out error
					);

					switch(NEType)
					{
						case NetworkEventType.Nothing:
						{
							print("Not recieving anything");
							yield return null;
							break;
						}
						case NetworkEventType.DataEvent:
						{
							OnDataEvent(recieveConnectionID, UnreliableChannel, recieveBuffer, recieveDataSize);
							break;
						}
						case NetworkEventType.ConnectEvent:
						{
							print($"Connect Event: {recieveSocketID}, {recieveConnectionID}, {recieveChannelID}");
							break;
						}
						case NetworkEventType.DisconnectEvent:
						{
							print($"Connect Event: {recieveSocketID}, {recieveConnectionID}, {recieveChannelID}");
							break;
						}
						default:
						{
							print($"LLAPI Client.Reciever -> { NEType.ToString() }");
							break;	
						}
					}
				}

			}

		#endregion

		#region Client Logic

			public void ConnectToServer()
			{
				_NetMessageContainer.MapMessages();

				GlobalConfig gConf = new GlobalConfig 
				{
					ThreadPoolSize = _ThreadPoolSize,
				};
				
				// Initializing the Transport Layer with default settings
				NetworkTransport.Init(gConf);

				ConnectionConfig connectionConfig = new ConnectionConfig
				{
					SendDelay = 0,
					MinUpdateTimeout = 1
				};
				ReliableChannel = connectionConfig.AddChannel(QosType.Reliable);
				UnreliableChannel = connectionConfig.AddChannel(QosType.Unreliable);

				// Maximum of 16 connections
				HostTopology hostTopolgy = new HostTopology(connectionConfig, 16);

				// 0 means that the OS assigns a port for us
				SocketID = NetworkTransport.AddHost(hostTopolgy, 0);

				byte error = 0;
				ServerConnectionID = NetworkTransport.Connect(SocketID, _ServerAddress, _ServerPort, 0, out error);

				if (error != 0)
				{
					Debug.LogErrorFormat("Failed connecting to server.");
				}

				Debug.LogFormat($"Client Details: {ReliableChannel}, {UnreliableChannel}, {SocketID}");
			}

			public void OnDataEvent(int connectionId, int channelId, byte[] data, int dataSize)
			{
				// Information about recieved data like size of the Message and the Ping
				print($"Received: {dataSize} bytes");
				
				// This temporary byterizer contains the msg received
				Byterizer tmpData = new Byterizer();
				tmpData.LoadDeep(data, dataSize);
				NetMessageType msgType = (NetMessageType)tmpData.PopByte();

				// The if statement Can be removed for final product if the message system is bug free and contained
				if (_NetMessageContainer.NetMessageMap.ContainsKey(msgType))
				{
					_NetMessageContainer.NetMessageMap[msgType].Client_ReceiveMessage(tmpData, this);
				}
				else
				{
					Debug.LogFormat($"Message not recognized [{msgType}]");
				}
			}

			private void DisconnectFromServer()
			{
				byte err;
				NetworkTransport.Disconnect(SocketID, ServerConnectionID, out err);
			}

			public void SendNetMessage(int channel, byte[] msgToSend)
			{
				byte err = 0;
				NetworkTransport.Send(SocketID, ServerConnectionID, channel, msgToSend, msgToSend.Length, out err);
				if (err != 0)
				{
					Debug.LogErrorFormat($"ERROR Sending Message -> Error {err}");			
				}
			}

		#endregion

		#region OnGUI

			private Queue<string> _Messages = new Queue<string>();
			
			public void AddMessageToQueue(string msg)
			{
				if (_Messages.Count > 20)
				{
					_Messages.Dequeue();
				}
				_Messages.Enqueue(msg);
			}

			private void OnGUI()
			{
				GUILayout.BeginHorizontal();
					GUILayout.BeginVertical();

						GUILayout.Label("\n");
						GUILayout.Label("Users connected:");
						GUILayout.Space(16);

						if(GUILayout.Button("Broadcast Hello"))
						{
							Byterizer b = new Byterizer();
							b.Push((byte)NetMessageType.BROADCAST);
							b.Push("Hello from "+ Username);
							SendNetMessage(ReliableChannel, b.GetBuffer());
						}

						foreach(var item in NetUsers)
						{
							if(GUILayout.Button(item.Value.UserName + " - " + item.Value.TeamNumber))
							{
								Byterizer b = new Byterizer();
								b.Push((byte)NetMessageType.WHISPER);
								b.Push(item.Value.ConnectionID);
								b.Push("Hello from "+ Username);
								SendNetMessage(ReliableChannel, b.GetBuffer());
							}
						}

					GUILayout.EndVertical();
					GUILayout.Space(16);
					GUILayout.BeginVertical();

						GUILayout.Label("Messages: ");
						GUILayout.Space(16);
						foreach (var msg in _Messages)
						{
							GUILayout.Label(msg);
						}

					GUILayout.EndVertical();

				GUILayout.EndHorizontal();
			}

		#endregion
	}
}

