﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;
using System.Text;

namespace LLAPI
{
	public class LLAPI_Server : MonoBehaviour 
	{
		#region Globals

			[SerializeField]
			private int _ServerPort = 27000;

			[SerializeField]
			private int _BufferSize = 1024;

			[SerializeField]
			private SO_NetMessageContainer _NetMessageContainer;

			private byte _ThreadPoolSize = 4;
			public int SocketID {get; private set;}
			public byte ReliableChannel {get; private set;}
			public byte UnreliableChannel {get; private set;}
			public Dictionary<int,NetUser> NetUsers = new Dictionary<int,NetUser>();

		#endregion

		#region Life Cycle
		
			void Start()
			{
				StartServer();
				StartCoroutine(Receiver());
			}

			private IEnumerator Receiver()
			{
				int recieveSocketID, recieveConnectionID, recieveChannelID, recieveDataSize;
				byte error;
				byte[] recieveBuffer = new byte[_BufferSize];

				while (true)
				{
					NetworkEventType NEType = NetworkTransport.Receive
					(
						out recieveSocketID, 
						out recieveConnectionID, 
						out recieveChannelID, 
						recieveBuffer, 
						_BufferSize, 
						out recieveDataSize, 
						out error
					);

					switch(NEType)
					{
						case NetworkEventType.Nothing:
						{
							yield return null;
							break;
						}
						case NetworkEventType.DataEvent:
						{
							OnDataEvent(recieveConnectionID, UnreliableChannel, recieveBuffer, recieveDataSize);
							break;
						}
						case NetworkEventType.ConnectEvent:
						{
							OnConnectEvent(recieveSocketID, recieveConnectionID);
							Debug.LogWarningFormat("Some one tried to connect to the server");
							break;
						}
						case NetworkEventType.DisconnectEvent:
						{
							OnDisconnectEvent(recieveSocketID, recieveConnectionID);
							break;
						}
						default:
						{
							print($"LLAPI Server.Reciever -> { NEType.ToString() }");
							break;	
						}
					}
				}

			}

		#endregion

		#region Server Logic

			public void StartServer()
			{
				_NetMessageContainer.MapMessages();

				GlobalConfig gConf = new GlobalConfig 
				{
					ThreadPoolSize = _ThreadPoolSize,
				};
				
				// Initializing the Transport Layer with default settings
				NetworkTransport.Init(gConf);

				ConnectionConfig connectionConfig = new ConnectionConfig
				{
					SendDelay = 0,
					MinUpdateTimeout = 1,
					ConnectTimeout = 5
				};
				ReliableChannel = connectionConfig.AddChannel(QosType.Reliable);
				UnreliableChannel = connectionConfig.AddChannel(QosType.Unreliable);

				// Maximum of 16 connections
				HostTopology hostTopolgy = new HostTopology(connectionConfig, 16);
				SocketID = NetworkTransport.AddHost(hostTopolgy, _ServerPort);

				Debug.LogFormat($"Server Details: {ReliableChannel}, {UnreliableChannel}, {SocketID}");
			}

			public void KickUser(int userToKick)
			{
				byte err;
				NetworkTransport.Disconnect(SocketID, userToKick, out err);
				NetUsers.Remove(userToKick);
			}

			public void OnDataEvent(int connectionID, int channelID, byte[] data, int dataSize)
			{
				// This temporary byterizer contains the msg received
				Byterizer tmpData = new Byterizer();
				tmpData.LoadDeep(data, dataSize);
				NetMessageType msgType = (NetMessageType)tmpData.PopByte();
				
				// The if statement Can be removed for final product if the message system is bug free and contained
				if (_NetMessageContainer.NetMessageMap.ContainsKey(msgType))
				{
					_NetMessageContainer.NetMessageMap[msgType].Server_ReceiveMessage(connectionID, tmpData, this);
				}
				else
				{
					Debug.LogFormat($"Message not recognized [{msgType}]");
				}
			}

			public void OnConnectEvent(int socketId, int connectionId)
			{
				NetUser tmp = new NetUser()
				{
					ConnectionID = connectionId,
				};
				if (NetUsers.ContainsKey(connectionId))
				{
					// User is reconnecting
					Debug.LogFormat($"User [{connectionId} - {NetUsers[connectionId].UserName}] Re-connected");
				}
				else
				{
					// New User
					NetUsers[connectionId] = tmp;
					
					Byterizer b = new Byterizer();
					b.Push((byte)NetMessageType.CONNECTION_ACK);
					b.Push(connectionId);

					SendNetMessage(connectionId, ReliableChannel, b.GetBuffer());
					Debug.LogFormat($"User [{connectionId}] Connected");
				}
				Debug.LogFormat($"User [{connectionId}] connected at Socket: {socketId}");
			}

			public void OnDisconnectEvent(int socketId, int connectionId)
			{
				Byterizer b = new Byterizer();
				b.Push((byte)NetMessageType.DISCONNECT_USER);
				b.Push(connectionId);

				BroadcastNetMessage(ReliableChannel, b.GetBuffer());

				if(NetUsers.ContainsKey(connectionId))
				{
					NetUsers.Remove(connectionId);
				}
				Debug.LogFormat($"User [{connectionId}] disconnected");
			}

			public void SendNetMessage(int connectionID, int channel, byte[] msgToSend)
			{
				byte err = 0;
				NetworkTransport.Send(SocketID, connectionID, channel, msgToSend, msgToSend.Length, out err);
				if (err != 0)
				{
					Debug.LogErrorFormat($"ERROR Sending Message -> Error {err}");			
				}
			}

			public void BroadcastNetMessage(int channel, byte[] msgToSend, int excludeId = -1)
			{
				foreach(var user in NetUsers)
				{
					if (user.Key != excludeId)
					{
						SendNetMessage(user.Key, channel, msgToSend);
					}
				}
			}

			public void MulticastNetMessage(int[] ids, int channel, byte[] msgToSend)
			{
				foreach(var id in ids)
				{
					SendNetMessage(id, channel, msgToSend);
				}
			}

		#endregion

		#region OnGUI
			private void OnGUI()
			{
				GUILayout.BeginHorizontal();
					GUILayout.BeginVertical();

						GUILayout.Label("\n");
						GUILayout.Label("Users connected:");
						GUILayout.Space(16);
						foreach(var item in NetUsers)
						{
							if(GUILayout.Button(item.Value.UserName + " - " + item.Value.TeamNumber))
							{
								print("Pressed : " + item.Value.UserName + " - " + item.Value.TeamNumber);
							}
						}

					GUILayout.EndVertical();
				GUILayout.EndHorizontal();
			}

		#endregion
	}
}


