﻿using System;
using UnityEngine;
using LLAPI;

[CreateAssetMenu(menuName = "LLAPI/Messages/UserInfo", fileName = "Message_UserInfo")]
public class Message_UserInfo : SO_ANetMessage
{
    public override void Client_ReceiveMessage(Byterizer data, LLAPI_Client client)
    {
        int connectionId = data.PopInt32();
		int teamNumber = data.PopInt32();
		String userName = data.PopString();

		NetUser tmp = new NetUser()
		{
			ConnectionID = connectionId,
			UserName = userName,
			TeamNumber = teamNumber,
		};

		// Registering or updating the user in the NetUsers Dictionary
		client.NetUsers[connectionId] = tmp;
    }

    public override void Server_ReceiveMessage(int clientConnectionId, Byterizer data, LLAPI_Server server)
    {
        server.NetUsers[clientConnectionId].UserName = data.PopString();
		server.NetUsers[clientConnectionId].TeamNumber = data.PopInt32();

		Byterizer b = new Byterizer();
		b.Push((byte)NetMessageType.USER_INFO);
		b.Push(clientConnectionId);
		b.Push(server.NetUsers[clientConnectionId].UserName);
		b.Push(server.NetUsers[clientConnectionId].TeamNumber);

		// Send my Info to all the other users
		server.BroadcastNetMessage(server.ReliableChannel, b.GetBuffer(), clientConnectionId);

		// Send the other users info to Me
		foreach (var item in server.NetUsers)
		{
			if (item.Key != clientConnectionId)
			{
				Byterizer b2 = new Byterizer();
				b2.Push((byte)NetMessageType.USER_INFO);
				b2.Push(item.Value.ConnectionID);
				b2.Push(item.Value.UserName);
				b2.Push(item.Value.TeamNumber);
				server.SendNetMessage(clientConnectionId, server.ReliableChannel, b.GetBuffer());	
			}
		}

		Debug.LogFormat($"ConnectionId: {clientConnectionId}, Username: {server.NetUsers[clientConnectionId].UserName}, Team: {server.NetUsers[clientConnectionId].TeamNumber}");

    }
}
