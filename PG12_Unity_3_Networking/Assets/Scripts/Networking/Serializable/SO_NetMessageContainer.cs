﻿using System.Collections.Generic;
using UnityEngine;

namespace LLAPI
{
	[CreateAssetMenu(menuName = "LLAPI/NetMessageContainer", fileName = "NetMessageContainer")]
	public class SO_NetMessageContainer : ScriptableObject 
	{
		[SerializeField]
		private SO_ANetMessage[] _NetMessages;

		public Dictionary<NetMessageType, SO_ANetMessage> NetMessageMap {get; private set;}

		public void MapMessages()
		{
			NetMessageMap = new Dictionary<NetMessageType, SO_ANetMessage>();
			foreach (var item in _NetMessages)
			{
				NetMessageMap[item.messageType] = item;
			}
		}
		
	}
}
