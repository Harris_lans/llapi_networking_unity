﻿using System.Collections;
using UnityEngine;
using LLAPI;

[CreateAssetMenu(menuName = "LLAPI/Messages/Broadcast", fileName = "Message_Broadcast")]
public class Message_Broadcast : SO_ANetMessage
{
    public override void Client_ReceiveMessage(Byterizer data, LLAPI_Client client)
    {
        int connectionId = data.PopInt32();
		string msg = data.PopString();

		client.AddMessageToQueue(client.NetUsers[connectionId].UserName + " : " + msg);
    }

    public override void Server_ReceiveMessage(int clientConnectionId, Byterizer data, LLAPI_Server server)
    {
        string msg = data.PopString();

		Byterizer b = new Byterizer();
		b.Push((byte)NetMessageType.BROADCAST);
		b.Push(clientConnectionId);
		b.Push(msg);
		
		// Do not broadcast the message to the person who asked you to broadcast
		server.BroadcastNetMessage(server.ReliableChannel, b.GetBuffer(), clientConnectionId);
    }
}
