﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LLAPI;

[CreateAssetMenu(menuName = "LLAPI/Messages/ConnectionAck", fileName = "Message_ConnectionAck")]
public class Message_ConnectionAck : SO_ANetMessage
{
	public override void Client_ReceiveMessage(Byterizer data, LLAPI_Client client)
	{
		int conId = data.PopInt32();
		NetUser tmp = new NetUser()
		{
			ConnectionID = conId,
			UserName = client.Username,
			TeamNumber = client.TeamNumber,
		};

		// Storing the NetUsers connected to the server
		client.NetUsers[conId] = tmp;

		Byterizer b = new Byterizer();
		b.Push((byte)NetMessageType.USER_INFO);
		b.Push(client.Username);
		b.Push(client.TeamNumber);

		client.SendNetMessage(client.ReliableChannel, b.GetBuffer());	
	}

	public override void Server_ReceiveMessage(int clientConnectionId, Byterizer data, LLAPI_Server server)
	{
		NetUser tmp = new NetUser()
		{
			ConnectionID = clientConnectionId,
		};
		if (server.NetUsers.ContainsKey(clientConnectionId))
		{
			// User is reconnecting
			Debug.LogFormat($"User [{clientConnectionId} - {server.NetUsers[clientConnectionId].UserName}] Re-connected");
		}
		else
		{
			// New User
			server.NetUsers[clientConnectionId] = tmp;
			
			Byterizer b = new Byterizer();
			b.Push((byte)NetMessageType.CONNECTION_ACK);
			b.Push(clientConnectionId);

			server.SendNetMessage(clientConnectionId, server.ReliableChannel, b.GetBuffer());
			Debug.LogFormat($"User [{clientConnectionId}] Connected");
		}
		Debug.LogFormat($"User [{clientConnectionId}] connected at Socket: {server.SocketID}");
	}
}
