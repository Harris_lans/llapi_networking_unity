﻿using UnityEngine;

namespace LLAPI
{
	public abstract class SO_ANetMessage : ScriptableObject 
	{
		public NetMessageType messageType;
		
		public abstract void Server_ReceiveMessage(int clientConnectionId, Byterizer data, LLAPI_Server server);
		public abstract void Client_ReceiveMessage(Byterizer data, LLAPI_Client client);
	}
}
