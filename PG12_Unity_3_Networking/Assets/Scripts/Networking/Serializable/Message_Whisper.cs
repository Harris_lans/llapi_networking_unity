﻿using System.Collections;
using UnityEngine;
using LLAPI;

[CreateAssetMenu(menuName = "LLAPI/Messages/Whisper", fileName = "Message_Whisper")]
public class Message_Whisper : SO_ANetMessage
{
    public override void Client_ReceiveMessage(Byterizer data, LLAPI_Client client)
    {
        int connectionId = data.PopInt32();
		string msg = data.PopString();

		client.AddMessageToQueue(client.NetUsers[connectionId].UserName + " : " + msg);
    }

    public override void Server_ReceiveMessage(int clientConnectionId, Byterizer data, LLAPI_Server server)
    {
        int destinationId = data.PopInt32();
		string msg = data.PopString();

		Byterizer b = new Byterizer();
		b.Push((byte)NetMessageType.WHISPER);
		b.Push(clientConnectionId);
		b.Push(msg);
		server.SendNetMessage(destinationId, server.ReliableChannel, b.GetBuffer());
    }
}
