﻿using UnityEngine;

namespace LLAPI
{
	public enum NetMessageType : byte
	{
		CONNECTION_ACK = 0,
		DISCONNECT_USER,
		USER_INFO,
		BROADCAST,
		WHISPER
	}
}

